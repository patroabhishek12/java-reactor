package com.example.reactiveprogramming.exception;

public class BookException extends RuntimeException{
    public BookException(String message) {
        super(message);
    }
}
