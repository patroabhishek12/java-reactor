package com.java9.flow;

import java.util.List;
import java.util.concurrent.Flow;
import java.util.concurrent.SubmissionPublisher;

public class ContentProducer implements Flow.Publisher{
    @Override
    public void subscribe(Flow.Subscriber subscriber) {
     // onsubscribe() method od subscriber is called here
        subscriber.onSubscribe(new ContentSubscriptions(subscriber));
    }

    public static void main(String[] args)
    {
        ContentProducer producer = new ContentProducer();
        ContentScubscriber subsciber = new ContentScubscriber(5);
        producer.subscribe(subsciber);
    }
}



