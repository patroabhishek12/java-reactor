package com.java9.flow;

import java.util.concurrent.Flow;
import java.util.concurrent.Flow.*;

public class MySubscriber <T> implements Flow.Subscriber<T> {
    private final String subscriberName;
    private Flow.Subscription subscription;

    public MySubscriber(String subscriberName)
    {
        this.subscriberName = subscriberName;
    }

    @Override
    public void onSubscribe(Subscription subscription) {
        this.subscription = subscription;
        subscription.request(2);
    }

    @Override
    public void onNext(T item) {
        try {
            System.out.println(Thread.currentThread().getName() + " >> Received Item: " + item);
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }
        subscription.request(2);
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println("Error while getting the requested item");
        throwable.printStackTrace();
    }

    @Override
    public void onComplete() {
        System.out.println("Completed");
    }
}
