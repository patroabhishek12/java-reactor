package com.java9.flow;

import java.util.concurrent.SubmissionPublisher;
import  java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

public class BasicFlowAPI {

    static AtomicBoolean isTrue = new AtomicBoolean();
    public static void main(String[] args)
    {
        List<Integer> integerList = List.of(1, 2, 3, 4, 5, 6, 7, 8 , 9, 10);

        //List<Integer> integerList = List.of();


        MySubscriber<Integer> mySubscriber = new MySubscriber<>("Subscriber1");

        SubmissionPublisher<Integer> integerPublisher = new SubmissionPublisher<>();

        integerPublisher.subscribe(mySubscriber);

        integerList.forEach(i -> {
            try{
                Thread.sleep(100);
            }catch (Exception ex){}
            isTrue.set(true);
            System.out.println(Thread.currentThread().getName() + " Publishing Item: " + i);
            integerPublisher.submit(i);
        });
        isTrue.set(false);
       integerPublisher.close();

        while(isTrue.get()); // once complete publishing (main-thread), it doesn't wait for consumer thread to complete so lets introduce a while loop


    }
}
