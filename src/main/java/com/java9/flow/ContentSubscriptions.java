package com.java9.flow;

import java.util.concurrent.Flow;

public class ContentSubscriptions  implements Flow.Subscription{

    private Flow.Subscriber contentScubscriber;

    public ContentSubscriptions(Flow.Subscriber contentScubscriber) {
        this.contentScubscriber = contentScubscriber;
    }

    @Override
    public void request(long n) {

        //onNext method od subscriber is called from here
        int i =0 ;
        while (i<n){
//            try{
//                Thread.sleep(1000);
//            }catch (Exception ex){}  //---> reduces speed of subscription
            contentScubscriber.onNext(i+1);
            i++;
        }

    }

    @Override
    public void cancel() {

    }
}