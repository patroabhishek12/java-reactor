package com.java9.flow;

import java.util.concurrent.Flow;

public class ContentScubscriber <T> implements Flow.Subscriber<T>{

    private long amountOfData; // controls the amount of data subscriber can handle at a given point of time

    public ContentScubscriber(long amountOfData) {
        this.amountOfData = amountOfData;
    }

    @Override
    public void onSubscribe(Flow.Subscription subscription) {
     subscription.request(amountOfData);
    }

    @Override
    public void onNext(T item) {
        try {
            System.out.println(Thread.currentThread().getName() + " >> Received Item: " + item);
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public void onError(Throwable throwable) {

    }

    @Override
    public void onComplete() {

    }
}
